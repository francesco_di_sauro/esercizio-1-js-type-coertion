let esercizioMaledetto = (![] + [])[+[]] + (![] + [])[+!+[]] + ([![]] + [][[]])[+!+[] + [+[]]] + (![] + [])[!+[] + !+[]];

console.log(esercizioMaledetto);

// lettera f

console.log((![] + [])[+[]]);

let a = ![]
let b = []
let c = +[]
let ab = a + b
let abc = (ab)[c]
console.log(a, typeof (a), b, typeof (b), c, typeof (c));
console.log(ab, typeof (ab), abc, typeof (abc));

let tiSpiego = ('false')[0]
console.log(tiSpiego);

// un buleano false viene sommato con un oggetto (array) vuoto ottenendo una stringa false
// della stringa false viene presa la lettera in posizione 0 overo la f

// lettera a

console.log((![] + [])[+!+[]]);

let d = ![];
let e = [];
let g = +!+[];
let de = d + e;
let deg = (de)[g];
console.log(d, typeof (d), e, typeof (e), g, typeof (g));
console.log(de, typeof (de), deg, typeof (deg));

// un buleano false viene sommato con un oggetto ottenendo una stringa false
// della stringa false viene presa la lettera in posizione 1

// lettera i

console.log(([![]] + [][[]])[+!+[] + [+[]]]);

let h = [![]]
let i = [][[]]
let l = +!+[]
let m = [+[]]
let hi = h + i
let lm = l + m
let hilm = (hi)[lm]

console.log(h, typeof(h),i, typeof(i),l, typeof(l),m, typeof(m));
console.log(hi, typeof(hi), lm, typeof(lm), hilm, typeof(hilm));

// un oggetto con al suo interno false viene sommato con undefined ottenendo una stringa falseundefined
// un numero viene sommato con un oggetto ottenendo la stringa 10
// della stringa falseundefined viene presa la lettera in posizione 10 

// lettera l

console.log((![] + [])[!+[] + !+[]]);

let n = ![]
let o = []
let p = !+[]
let q = !+[]
let no = n + o
let pq = p + q
let nopq = (no)[pq]

console.log(n, typeof(n),o, typeof(o),p, typeof(p),q, typeof(q));
console.log(no, typeof(no), pq, typeof(pq), nopq, typeof(nopq));

// un buleano false viene sommato con un oggetto ottenendo una stringa false
// un buleano true viene sommato con un buleano true ottenendo il numero 2
// della stringa false viene presa la lettera in posizione 2 
